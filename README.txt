
Rules Extra Tokens
==================

A tiny module that contains a Rules Action to make the contents of a cookie
or session variable available as a token, [cookie:value] or [session:value], 
for use in subsequent Actions in the same Rule.

This module is very much related to and could (should?) be merged with:

drupal.org/project/rules_cookie
drupal.org/project/cookie_monster
drupal.org/project/rules_session_vars
