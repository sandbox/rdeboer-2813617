<?php
/**
 * @file
 * Rules module Actions integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_extra_tokens_rules_action_info() {
  $items['cookie'] = array(
    'label' => t('Provide cookie value'),
    'base' => 'rules_extra_tokens_cookie',
    'group' => t('Data'),
    'parameter' => array(
      'name' => array(
        'type' => 'text',
        'label' => 'Cookie name',
        'description' => 'The name of the cookie whose value you wish to retrieve. Provides an empty value, if the cookie does not exist.',
        'optional' => FALSE,
      )
    ),
    'provides' => array(
      'cookie' => array(
        'type' => 'text',
        'label' => t('Cookie value'),
      ),
    ),
  );

  $items['session'] = array(
    'label' => t('Provide session value'),
    'base' => 'rules_extra_tokens_session',
    'group' => t('Data'),
    'parameter' => array(
      'name' => array(
        'type' => 'text',
        'label' => 'Session variable name',
        'description' => 'The name of the session variable whose value you wish to retrieve. Provides an empty value, if the variable name does not exist.',
        'optional' => FALSE,
      )
    ),
    'provides' => array(
      'session' => array(
        'type' => 'text',
        'label' => t('Session value'),
      ),
    ),
  );

  return $items;
}

/**
 * Callback for cookie token action.
 */
function rules_extra_tokens_cookie($name) {
  return array('cookie' => isset($_COOKIE[$name]) ? $_COOKIE[$name] : '');
}

/**
 * Callback for session token action.
 */
function rules_extra_tokens_session($name) {
   return array('session' => isset($_SESSION[$name]) ? $_SESSION[$name] : '');
}
